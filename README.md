```
____    __    ____  __  .__   __.  _______   _______     _______. __    __   _______  __  .___  ___.
\   \  /  \  /   / |  | |  \ |  | |       \ |   ____|   /       ||  |  |  | |   ____||  | |   \/   |
 \   \/    \/   /  |  | |   \|  | |  .--.  ||  |__     |   (----`|  |__|  | |  |__   |  | |  \  /  |
  \            /   |  | |  . `  | |  |  |  ||   __|     \   \    |   __   | |   __|  |  | |  |\/|  |
   \    /\    /    |  | |  |\   | |  '--'  ||  |____.----)   |   |  |  |  | |  |____ |  | |  |  |  |
    \__/  \__/     |__| |__| \__| |_______/ |_______|_______/    |__|  |__| |_______||__| |__|  |__|
```
## DisjointSets

### Lecture 1: Implement the DisjointSets class

Get the source.
```bash
$ git init
$ git remote add origin https://bitbucket.org/windesheim-se-gp/disjointsets-cpp.git
$ git pull origin lecture-1
```

implement the following class:

```c++

    class DisjointSets {

    public:
        DisjointSets(int size);
        ~DisjointSets();

        int find_root(int p) const;
        void joint(int p, int q);
        int size() const;
        int size_of(int p) const;

    private:
        int length;
        int* N;

    };

```

When the asserts pass you're good to go.

### Lecture 2: Implement the Maze and MazeFormatter classes

To get the headerfile updates
```bash
$ git pull origin lecture-2
```
The classes are as follows:
```c++

    class Maze {

    public:
        Maze(int width, int height);
        ~Maze();
        bool is_open(const int, const bool) const;
        int get_size() const;
        int get_width() const;
        int get_height() const;

    private:
        int width, height, size, edge_count;
        std::pair<bool, bool>*edges;
        void next_relation(int &p, int &q, short &d);

    };

    class MazeFormatter {

    public:
        virtual std::string format(const Maze&) const;

    };

```